﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreMenuController : SimpleMenuController
{


  
    public void AA_SetScoreText (float newScore, float maxScore)
    {
        foreach (Text item in m_scoreTexts)
            item.text = newScore.ToString ("N0");

        foreach (Text item in m_bestDistanceText)
            item.text = maxScore.ToString("N0");

        Debug.Log("====================================================");
        Debug.Log("ZZZZZZZ>>>> thisDistance " + newScore);
        Debug.Log("ZZZZZZZ>>>> maxDistance " + maxScore);
        Debug.Log("====================================================");
    }


    
    [SerializeField]
    Text[] m_scoreTexts;
    [SerializeField]
    Text[] m_bestDistanceText;
}
