﻿using GooglePlayGames;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class PSS_Stall : PSS_PlayerShipState
{
    public override void Enter()
    {
        m_target.AA_SetShipToStall();
                                                        
        IncrementarLeaderboard("CgkI0vvp1LgMEAIQAA", m_target.m_totalDistance);

        float lastMaxDistance = Mathf.Max ( PlayerPrefs.GetFloat("CurrentBestDistance"), m_target.m_totalDistance);

        m_gameController.SM_GoToScoreMenu(m_target.m_totalDistance, lastMaxDistance);
        m_target.m_references.m_smoke.Play();
        
    }

    public void IncrementarLeaderboard(string boardId, float valueToPush)
    {


        #region test for retreive data from leaderboard
        ILeaderboard lb = PlayGamesPlatform.Instance.CreateLeaderboard();
        lb.id = boardId;
        UnityEngine.Debug.Log("========================================");
        UnityEngine.Debug.Log("========================================");
        UnityEngine.Debug.Log("========================================");

        float lbValue = 0;
        lb.LoadScores(ok =>
        {
            if (ok)
            {
                // LoadUsersAndDisplay(lb);
                UnityEngine.Debug.Log("###########>>>> " + lb.localUserScore.value.ToString());
                UnityEngine.Debug.Log("###########>>>> " + lb.localUserScore.value.ToString());
                UnityEngine.Debug.Log("###########>>>> " + lb.localUserScore.value.ToString());

                Social.ReportScore((int)valueToPush, boardId, (bool success) => {
                    // handle success or failure
                });

                lbValue = lb.localUserScore.value;


                
            }
            else
            {
                Debug.Log("Error retrieving leaderboardi");
            }

           
        });
        #endregion

        



    }

    public override void Exit()
    {
        m_target.m_references.m_smoke.Stop();
        //throw new NotImplementedException();
    }

    public override void FixedUpdate()
    {
        //throw new NotImplementedException();
    }

    public override void Update()
    {
        //throw new NotImplementedException();
    }

    private void Awake()
    {
        m_gameController = FindObjectOfType<GameController>();        
    }

    GameController m_gameController;
}
